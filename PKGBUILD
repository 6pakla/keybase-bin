# Maintainer: Keybase <linux-packages@keyba.se>

# NOTE: This PKGBUILD is generated and pushed by Keybase's release automation.
# Any changes made in aur.archlinux.org git repos will get overwritten. See
# https://github.com/keybase/client/tree/master/packaging/linux/arch.

pkgname=keybase-bin
pkgdesc='the Keybase Go client, filesystem, and GUI'
license=('BSD')
url='https://keybase.io'
pkgver=5.3.0_20200310205642+4f2689009b
src_prefix=https://prerelease.keybase.io/linux_binaries/deb
deb_pkgver="${pkgver/_/-}"
deb_pkgver="${deb_pkgver/+/.}"
pkgrel=1
arch=('i686' 'x86_64')
depends=(fuse gconf libxss gtk2 lsof) # don't change this without changing the SRCINFO template too
                                      # also make sure to change the keybase-git PKGBUILD
provides=(keybase keybase-gui kbfs)
# keybase-release is a deprecated AUR package
conflicts=(keybase keybase-release keybase-git keybase-gui kbfs)
source_i686=(
  "${src_prefix}/keybase_${deb_pkgver}_i386.deb"
)
source_x86_64=(
  "${src_prefix}/keybase_${deb_pkgver}_amd64.deb"
)
install=keybase.install

package() {
  if [ "$CARCH" = "i686" ] ; then
    deb_arch="i386"
  elif [ "$CARCH" = "x86_64" ] ; then
    deb_arch="amd64"
  else
    echo "Unknown arch: $CARCH"
    exit 1
  fi

  cd "$srcdir"
  deb_package="keybase_${deb_pkgver}_${deb_arch}.deb"
  ar xf "$deb_package"
  tar xf data.tar.xz -C "$pkgdir"

  # Omit the cronjobs that the Debian package includes.
  rm -rf "$pkgdir/etc/cron.daily"
}

# You can cross reference these hashes with Keybase Debian repo metadata:
# https://prerelease.keybase.io/deb/dists/stable/main/binary-amd64/Packages
# https://prerelease.keybase.io/deb/dists/stable/main/binary-i386/Packages
sha256sums_i686=(3e955031f65715692ab9fce072169f3cbfd0cb7da50da0fcc45cba4cc034fc2b)
sha256sums_x86_64=(61cb6a6d4548b77d21b893aaa4fe3e42063dd6f8de4a8dbb61668e319be05a7d)
