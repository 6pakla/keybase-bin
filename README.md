# keybase-bin

The Keybase Go client, filesystem, and GUI

https://keybase.io

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/rebornos-packages/keybase-bin.git
```
